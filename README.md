# Blockchain-Based Delivery Drone System

## Build PX4-Autopilot

### Build (Using the jMAVSim Simulator)

First we'll build a simulated target using a console environment. This allows us to validate the system setup before moving on to real hardware and an IDE.

Navigate into the PX4-Autopilot directory and start jMAVSim using the following command:



    $ cd @PROJECT_FOLDER@/blockchain-based-delivery-drone-system
    $ git submodule update --init --recursive
    $ cd PX4-Autopilot
    $ bash ./Tools/setup/ubuntu.sh
    $ make px4_sitl jmavsim

The drone can be flown by typing:

    $ pxh> commander takeoff

### Build (Using the Gazebo Simulator)

    $ cd ~//blockchain-based-delivery-drone-system/PX4-Autopilot
    $ make px4_sitl gazebo

## Install MAVSDK

First, download the prebuilt C++ [MAVSDK library version 1.0.8](https://github.com/mavlink/MAVSDK/releases/tag/v1.0.8)



## Usage
WIP

